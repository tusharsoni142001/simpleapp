const express=require('express')
const fs=require('fs')
const app=express()
const port=3003


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

const server=app.listen(port,()=>{
    console.log(`Application is running at port: ${port}`)
})

app.get('/form',(req,res)=>{
    res.sendFile(__dirname+'/public/index.html');
})

app.post('/form',(req,res)=>{
    const { username, email, age, phoneno } = req.body;
    const formData = `Username: ${username}\nEmail: ${email}\nAge: ${age}\nPhone Number: ${phoneno}\n\n`;

    fs.writeFile('formData.txt',formData,(err)=>{
        if(err){
            console.log(err);
        }
        console.log('Form data saved successfully');
    });
    console.log(req.body);
    res.sendFile(__dirname+'/public/formData.html');

    server.close(() => {
        console.log('Server stopped');
    });
    
});
